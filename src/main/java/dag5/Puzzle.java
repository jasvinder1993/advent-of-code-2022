package dag5;

import utils.Utils;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Puzzle {

    private static final int dag = 5;

    public static void main(String[] args) {

        Iterator<String> it = Utils.getInputLines(dag, false).toList().iterator();
        List<Deque<String>> piles = new ArrayList<>();

        // Read initial situation
        Pattern p = Pattern.compile("\\[([A-Z]+)\\]");
        while(true) {
            String s = it.next();

            if (s.startsWith(" 1")) {
                break;
            }

            for(int i = 0; i < ((s.length() + 1) / 4); i++) {
                String ss = s.substring(i * 4, i * 4 + 3);
                Matcher m = p.matcher(ss);

                if (i >= piles.size()) {
                    piles.add(new ArrayDeque<>());
                }

                Deque<String> pile = piles.get(i);
                if(m.matches()) {
                    pile.addLast(m.group(1));
                }
            }
        }

        it.next();

        // Read operations
        Pattern p2 = Pattern.compile("move (\\d+) from (\\d+) to (\\d+)");
        while(it.hasNext()) {
            String s = it.next();
            Matcher m = p2.matcher(s);
            m.find();
            int nr = Integer.parseInt(m.group(1));
            int from = Integer.parseInt(m.group(2));
            int to = Integer.parseInt(m.group(3));

            for(int k = 0; k < nr; k++) {
                String sc = piles.get(from - 1).pollFirst();
                piles.get(to - 1).addFirst(sc);
            }
        }

        // Make output
        String output = piles.stream().map(Deque::peekFirst).reduce("", String::concat);
        System.out.println(output);
    }
}
