package dag8;

import utils.Utils;

import java.awt.geom.Point2D;
import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Puzzle {

    private static final int dag = 8;

    public static void main(String[] args) {

        List<List<Integer>> m = new ArrayList<>();
        for(String s : Utils.getInputLines(dag, false).toList()) {
            m.add(new ArrayList<>());
            for(String s2 : s.split("")) {
                m.get(m.size() - 1).add(Integer.parseInt(s2));
            }
        }

        int width = m.get(0).size();
        int height = m.size();

        Stream<Stream<Point>> st = Stream.empty();
        st = Stream.concat(st, IntStream.range(0, height).mapToObj(y -> IntStream.range(0, width).mapToObj(x -> new Point(x, y))));
        st = Stream.concat(st, IntStream.range(0, height).mapToObj(y -> IntStream.range(0, width).mapToObj(x -> new Point(width - 1 - x, y))));
        st = Stream.concat(st, IntStream.range(0, width).mapToObj(x -> IntStream.range(0, height).mapToObj(y -> new Point(x, y))));
        st = Stream.concat(st, IntStream.range(0, width).mapToObj(x -> IntStream.range(0, height).mapToObj(y -> new Point(x, height - 1 - y))));

        Boolean[][] check = new Boolean[height][width];
        for(Stream<Point> sp : st.toList()) {
            int heighest = -1;
            for(Point p : sp.toList()) {
                int h = m.get(p.y).get(p.x);
                if (h > heighest) {
                    heighest = h;
                    check[p.y][p.x] = true;
                }
            }
        }

        long total = Arrays.stream(check).flatMap(Arrays::stream).filter(b -> b != null).count();
        System.out.println(total);
    }

    private static class Point {
        private final int x;
        private final int y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
}
