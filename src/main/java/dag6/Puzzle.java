package dag6;

import utils.Utils;

import java.util.*;

public class Puzzle {

    private static final int dag = 6;

    public static void main(String[] args) {

        Deque<String> de = new ArrayDeque<>();
        int i = 0;
        for(String s : Utils.getInputLines(dag, false).findFirst().get().split("")) {
            i++;
            if (de.contains(s)) {
                while(!de.pollLast().equals(s)) { }
            }
            de.addFirst(s);
            if (de.size() == 14){
                break;
            }
        }

        System.out.println(i);
    }
}
