package dag17;

import com.google.common.collect.EvictingQueue;
import org.apache.commons.lang3.tuple.Pair;
import utils.Utils;

import java.util.*;
import java.util.stream.Collectors;

public class Puzzle2 {

    private static final int dag = 17;

    private record Point(int x, long y) { }

    private static final List<List<Point>> shapes = Arrays.asList(
            Arrays.asList(new Point(0,0 ), new Point(1,0 ), new Point(2,0 ), new Point(3,0 )),
            Arrays.asList(new Point(1,0 ), new Point(0,1 ), new Point(1,1 ), new Point(2,1 ), new Point(1,2 )),
            Arrays.asList(new Point(0,0), new Point(1,0), new Point(2,0 ), new Point(2,1 ), new Point(2,2 )),
            Arrays.asList(new Point(0,0), new Point(0,1), new Point(0,2 ), new Point(0,3 )),
            Arrays.asList(new Point(0,0), new Point(1,0), new Point(0,1 ), new Point(1,1 ))
            );

    public static void main(String[] args) {

        long totalNrRocks = 1000000000000L;

        long shapeCnt = 0;
        long maxRockHeight = 0;
        Set<Point> curShape = move(new HashSet<>(shapes.get(0)), new Point(2, 3));
        Set<Point> fixed = new HashSet<>();

        List<String> windDirs = Arrays.stream(Utils.getInputLines(dag, false).toList().get(0).split("")).toList();

        Map<Integer, Map<List<Set<Point>>, Pair<Long, Long>>> history = new HashMap<>();
        EvictingQueue<Set<Point>> lastShapesQueue = EvictingQueue.create(2000);
        long maxRockHeightExtra = 0;
        while(true) {
            int c = 0;
            for (String s : windDirs) {
                c++;

                // Hor
                Set<Point> curShapePot = move(curShape, new Point(s.equals(">") ? 1 : -1, 0));
                if (curShapePot.stream().allMatch(p -> p.x >= 0 && p.x < 7 && !fixed.contains(p)))
                    curShape = curShapePot;

                // Ver
                curShapePot = move(curShape, new Point(0, -1));
                if (curShapePot.stream().allMatch(p -> p.y >= 0 && !fixed.contains(p))) {
                    curShape = curShapePot;
                } else { // Stopped
                    fixed.addAll(curShape);
                    maxRockHeight = Math.max(maxRockHeight, curShape.stream().mapToLong(sh -> sh.y).max().getAsLong() + 1);
                    lastShapesQueue.add(move(curShape, new Point(0, -maxRockHeight)));

                    List<Set<Point>> lastShapes = new ArrayList<>(lastShapesQueue);
                    if (maxRockHeightExtra == 0 && lastShapesQueue.remainingCapacity() == 0) {
                        if (history.computeIfAbsent(c, d -> new HashMap<>()).containsKey(lastShapes)) {
                            long rockHeightDiff = maxRockHeight - history.get(c).get(lastShapes).getKey();
                            long nrRocksDiff = shapeCnt - history.get(c).get(lastShapes).getValue();
                            long repeatNr = (totalNrRocks - shapeCnt) / nrRocksDiff;
                            shapeCnt += repeatNr * nrRocksDiff;
                            maxRockHeightExtra = repeatNr * rockHeightDiff;
                        } else {
                            history.get(c).put(lastShapes, Pair.of(maxRockHeight, shapeCnt));
                        }
                    }

                    curShape = new HashSet<>(shapes.get((int)(++shapeCnt % shapes.size())));

                    if (shapeCnt == totalNrRocks) {
                        System.out.println(maxRockHeight + maxRockHeightExtra);
                        return;
                    }

                    curShape = move(curShape, new Point(2, maxRockHeight + 3));
                }
            }
        }
    }

    private static Set<Point> move(Set<Point> shape, Point delta) {
        return shape.stream().map(p -> new Point(p.x + delta.x, p.y + delta.y)).collect(Collectors.toSet());
    }
}
